package com.edsonjr.picpay.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edsonjr.picpay.model.User
import com.edsonjr.picpay.repository.Repositories
import com.edsonjr.picpay.repository.remote.UserRemoteRepository
import com.edsonjr.picpay.service.NetworkResult
import kotlinx.coroutines.launch

class UserViewModel(private val repositories: Repositories): ViewModel() {

    private val TAG = this.javaClass.name

    private val _getAllUsersViewState = MutableLiveData<ViewState<List<User>>>()
    val getAllUsersViewState: LiveData<ViewState<List<User>>>
        get() = _getAllUsersViewState



    
    fun getRemoteUsers() = viewModelScope.launch {
        _getAllUsersViewState.value = ViewState.LOADING
        val networkResult  = repositories.remote.getAllUsers()

        when(networkResult){
            is NetworkResult.Success -> {
                Log.d(TAG,"Get users returned successfully")
                _getAllUsersViewState.postValue(ViewState.SUCCESS(networkResult.data))
            }
            is NetworkResult.Error -> {
                Log.e(TAG,"Error when get users")
                Log.e(TAG,networkResult.errorMsg.toString())

            }
        }
    }
}