package com.edsonjr.picpay.di

import com.edsonjr.picpay.datasource.remote.UserRemoteDataSource
import com.edsonjr.picpay.datasource.remote.UserRemoteDataSourceImpl
import com.edsonjr.picpay.repository.Repositories
import com.edsonjr.picpay.repository.remote.UserRemoteRepository
import com.edsonjr.picpay.repository.remote.UserRemoteRepositoryImpl
import com.edsonjr.picpay.service.PicPayService
import com.edsonjr.picpay.service.RetrofitService
import com.edsonjr.picpay.viewmodel.UserViewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object di {
    private val retrofitServiceModule = module {
        single { RetrofitService().initRetrofit()}
        single <PicPayService>  {get<Retrofit>().create((PicPayService::class.java))}

    }


    private val viewModelModule = module{
        single  { UserViewModel(get())}
    }

    private val dataSourceModule = module {
        single  <UserRemoteDataSource> { UserRemoteDataSourceImpl(get()) }
    }



    private val repositoryModule = module {
        single <UserRemoteRepository> { UserRemoteRepositoryImpl(get())  }
        single { Repositories(get()) }
    }


    //list of all modules
    val appModules = listOf(
        retrofitServiceModule,
        dataSourceModule,
        repositoryModule,
        viewModelModule)
}
