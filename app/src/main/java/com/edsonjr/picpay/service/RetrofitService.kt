package com.edsonjr.picpay.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitService {

    private val base_url = "https://609a908e0f5a13001721b74e.mockapi.io/picpay/api/"

    private var logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val client = OkHttpClient.Builder()

    fun initRetrofit (): Retrofit {

        //configure logging for retrofit 2
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        client.addInterceptor(logging)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()


        return Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }


}