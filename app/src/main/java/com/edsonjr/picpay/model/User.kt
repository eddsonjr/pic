package com.edsonjr.picpay.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class User(
    @SerializedName("img") val img: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("id") val id: Int? = null,
    @SerializedName("username") val username: String? = null
) : Serializable