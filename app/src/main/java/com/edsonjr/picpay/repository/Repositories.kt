package com.edsonjr.picpay.repository

import com.edsonjr.picpay.datasource.local.UserLocalDataSourceImpl
import com.edsonjr.picpay.datasource.remote.UserRemoteDataSource
import com.edsonjr.picpay.datasource.remote.UserRemoteDataSourceImpl
import com.edsonjr.picpay.repository.remote.UserRemoteRepositoryImpl
import com.edsonjr.picpay.service.PicPayService


//TODO - Adicionar uma nova variavel para receber o dao do room e passar ela como parametro no local
class Repositories(
    private val remoteDataSource: UserRemoteDataSource) {

    val remote = UserRemoteRepositoryImpl(remoteDataSource) //datasource remoto

    //TODO - Passar o DAO para o repositorio local
    val local = UserLocalDataSourceImpl() //datasource local - Room


}