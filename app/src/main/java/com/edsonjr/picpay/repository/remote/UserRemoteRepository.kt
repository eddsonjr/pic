package com.edsonjr.picpay.repository.remote

import androidx.lifecycle.LiveData
import com.edsonjr.picpay.model.User
import com.edsonjr.picpay.service.NetworkResult

interface UserRemoteRepository {

   suspend fun getAllUsers(): NetworkResult<List<User>>


}