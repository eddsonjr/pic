package com.edsonjr.picpay.repository.remote

import android.util.Log
import com.edsonjr.picpay.datasource.remote.UserRemoteDataSource
import com.edsonjr.picpay.model.User
import com.edsonjr.picpay.service.BaseApiResponse
import com.edsonjr.picpay.service.NetworkResult

class UserRemoteRepositoryImpl(private val remoteDataSource: UserRemoteDataSource):
    UserRemoteRepository, BaseApiResponse() {

    private val TAG = this.javaClass.name

    override suspend fun getAllUsers(): NetworkResult<List<User>> {
        Log.d(TAG, "Calling getUsers for remote data source...")
        val response = remoteDataSource.getUsers()
        return safeApiCall { response }
    }
}