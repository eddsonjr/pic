package com.edsonjr.picpay

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.picpay.databinding.ActivityMainBinding
import com.edsonjr.picpay.model.User
import com.edsonjr.picpay.view.UserListAdapter
import com.edsonjr.picpay.viewmodel.UserViewModel
import com.edsonjr.picpay.viewmodel.ViewState
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val TAG = this.javaClass.name
    private lateinit var binding: ActivityMainBinding
    private val userViewModel: UserViewModel by viewModel()
    private val adapter =  UserListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Chamando o servico
        userViewModel.getRemoteUsers()


        userViewModel.getAllUsersViewState.observe(this) { viewState ->
            when(viewState) {
                is ViewState.LOADING -> {
                    Log.d(TAG, "Carregando usuarios")
                    binding.userListProgressBar.visibility = View.VISIBLE
                }

                is ViewState.SUCCESS -> {
                    Log.d(TAG,"Retornado com sucesso")
                    binding.userListProgressBar.visibility = View.GONE
                    viewState.data?.let { setupAdapter(it) }
                }
                is ViewState.ERROR -> {
                    Log.d(TAG, "Falha na requisicao")
                    //TODO - COLOCAR VIEW DE ERRO
                }
            }
        }
    }

    private fun setupAdapter(users: List<User>) {
        Log.d(TAG, "Configurando adapter...")
        adapter.users = users
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)



    }
}