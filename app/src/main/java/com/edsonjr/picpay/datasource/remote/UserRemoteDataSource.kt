package com.edsonjr.picpay.datasource.remote

import com.edsonjr.picpay.model.User
import com.edsonjr.picpay.service.NetworkResult
import retrofit2.Response

interface UserRemoteDataSource {

    suspend fun getUsers(): Response<List<User>>
}