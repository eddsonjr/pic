package com.edsonjr.picpay.datasource.remote

import android.util.Log
import com.edsonjr.picpay.model.User
import com.edsonjr.picpay.service.PicPayService
import retrofit2.Response

class UserRemoteDataSourceImpl(private val picPayService: PicPayService): UserRemoteDataSource {


    private val TAG = this.javaClass.name

    override suspend fun getUsers(): Response<List<User>>{
        Log.d(TAG,"Calling get users from API")
        return picPayService.getUsers()
    }

}